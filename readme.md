## Setup

Make sure you have python 2.7 and `virtualenv` installed.

Run these commands (the last one will take a while!):

    virtualenv venv
    . ./venv/bin/activate
    pip install -r requirements.txt

Then your python files will have access to the z3 library. You can run the programs as e.g.

    python integer_inequality.py

You can also follow `integer_inequality.py` as a template for other constraint problems, by copying it and replacing the variable definitions, constraints, and outputs.
