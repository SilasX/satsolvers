import z3.z3 as z
from z3 import Optimize
from z3.z3types import Z3Exception

# Maximize 12x + 40y subject to
# x + y <= 16
# x + 3y <= 36
# x <= 10
# x >= 0
# y >= 0

# Initialize the solver
opt = Optimize()

# Define variables and their types
x = z.Real('x')
y = z.Real('y')
value = z.Real('value')

# Add constraints

opt.add(value == 12*x + 40*y)
opt.add(x >= 0)
opt.add(x <= 10)
opt.add(y >= 0)
opt.add(x + y <= 12)
opt.add(x + 3*y <= 36)

# Maximize the value
opt.maximize(value)
opt.check()
try:
    model = opt.model()
    xout = model.eval(x).as_long()
    yout = model.eval(y).as_long()
    valueout = model.eval(value).as_long()
    print "x = {}, y = {}, value = {}".format(xout, yout, valueout)

except Z3Exception:
    print "UNSAT"

