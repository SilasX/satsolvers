import z3.z3 as z
from z3.z3types import Z3Exception
import sys

# Calculation:

# words1-4 are two bytes
# x1-4 and y1-4 are each one byte

#  y1x1  (word1)
# +y2x2  (word2)
#------
#  f2f1

#  f2f1
# +y3x3  (word3)
#------
#  g2g1

#  g2g1  
# +y4x4  (word4)
#------
#  h2h1

# h2h1 must equal target; y4x4 and y3x3 and y2x2 may equal 0

for target in xrange(0x10000):
    # Ignore everything where the target itself has 5th bit set; we don't care
    # about those
    if target % 32 >= 16:
        continue
    slv = z.Solver()
    

    bytedata = z.BitVecSort(8)
    worddata = z.BitVecSort(16)
    wordvars = ["word1", "word2", "word3", "word4"]
    word1, word2, word3, word4 = tuple(
        [z.BitVec(q, worddata) for q in wordvars]
    )
    bytevars = ["x1", "y1", "x2", "y2", "x3", "y3", "x4", "y4"]
    x1, y1, x2, y2, x3, y3, x4, y4 = tuple(
        [z.BitVec(q, bytedata) for q in bytevars]
    )

    # words are combinations of bytes
    slv.add(word1 == z.Concat(y1, x1))
    slv.add(word2 == z.Concat(y2, x2))
    slv.add(word3 == z.Concat(y3, x3))
    slv.add(word4 == z.Concat(y4, x4))
    
    # Alphanum range = 0x30-0x39, 0x41-0x5a, 0x61=0x7a
    # Each word must be in alphanum range or zero
    # you don't need to add a second or third time.

    slv.add(
        z.And([z.Or([
            z.And([q >= 0x30, q <= 0x39]),
            z.And([q >= 0x41, q <= 0x5a]),
            z.And([q >= 0x61, q <= 0x7a])])
            for q in [x1, y1]
        ])
    )
    # All the others have to have the bytes be 0, or within the range
    slv.add(
        z.Or([
            word2 == 0,
            z.And([z.Or([
                z.And([q >= 0x30, q <= 0x39]),
                z.And([q >= 0x41, q <= 0x5a]),
                z.And([q >= 0x61, q <= 0x7a])])
                for q in [x2, y2]
            ]),
        ])
    )

    slv.add(
        z.Or([
            word3 == 0,
            z.And([z.Or([
                z.And([q >= 0x30, q <= 0x39]),
                z.And([q >= 0x41, q <= 0x5a]),
                z.And([q >= 0x61, q <= 0x7a])])
                for q in [x3, y3]
            ]),
        ])
    )

    slv.add(
        z.Or([
            word4 == 0,
            z.And([z.Or([
                z.And([q >= 0x30, q <= 0x39]),
                z.And([q >= 0x41, q <= 0x5a]),
                z.And([q >= 0x61, q <= 0x7a])])
                for q in [x4, y4]
            ]),
        ])
    )

    
    # Sum of words must equal target
    slv.add(word1 + word2 + word3 + word4 == target)

    # Additional!
    bitmask5 = 32
    # Sr's 5th bit can't be set i.e. x1, f1, g1 % 32 < 16.
    slv.add(z.And([
        q & bitmask5 == 0 for q in [word1, word1 + word2, word1 + word2 + word3]
    ]))

    #print "target = {0:04x}".format(target)
    slv.check()
    try:
        model = slv.model()
        word1out = model.eval(word1).as_long()
        word2out = model.eval(word2).as_long()
        word3out = model.eval(word3).as_long()
        word4out = model.eval(word4).as_long()
        print"{:04x}: {:04x}, {:04x}, {:04x}, {:04x}".format(
            target, word1out, word2out, word3out, word4out
        )
    except Z3Exception:
        print "{:04x}: UNSAT".format(target)
    if target % 1000 == 0:
        sys.stdout.flush()
