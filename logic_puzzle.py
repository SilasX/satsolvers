import z3.z3 as z
from z3 import Distinct
from z3.z3types import Z3Exception


# Initialize the solver
slv = z.Solver()

SIZE = 4
# Define variables and their types
# Array for Town-contestant assignments: 0=Eustis, 1=Fowler, 2=Lytton, 3=Stanton
T = [z.Int('t{}'.format(i)) for i in xrange(SIZE)]
# Array for Contestants-placement assignments: 0=Kelvin, 1=Leona, 2=Willie, 3=Yvonne
# first place = 0, 4th place = 3
C = [z.Int('c{}'.format(i)) for i in xrange(SIZE)]
## Array for Placements-town assignments:
P = [z.Int('p{}'.format(i)) for i in xrange(SIZE)]

# Add constraints

# Each entry can only be 0-3
for i in xrange(SIZE):
    slv.add(T[i] >= 0)
    slv.add(T[i] <= 3)
    slv.add(C[i] >= 0)
    slv.add(C[i] <= 3)
    slv.add(P[i] >= 0)
    slv.add(P[i] <= 3)

# Each array uniquely assigns solutions (each contestant gets exactly one of the
# placements)
slv.add(Distinct(*T))
slv.add(Distinct(*C))
slv.add(Distinct(*P))

#1. The contestant from Fowler finished 2 places before Willie.
# Thus, either Willie was 3 and Fowler was 1, or Willie was 2 and Fowler was 0
slv.add(
    z.Or([
        # Contestant 2 (Willie) is place 3 (4th), place 1er (2nd) is Fowler (1)
        z.And([C[2] == 3, P[1] == 1]),  
        # Contestant 2 (Willie) is place 2 (3rd), place 0er (1st) is Fowler (1)
        z.And([C[2] == 2, P[0] == 1]),
    ])
)

#2. The solver from Eustis finished 1 place after the contestant from Lytton.
# Thus, either Lytton was 0 and Eustis was 1, or Lytton was 1 and Eustis was 2,
# or Lytton was 2 and Eustus was 3
slv.add(
    z.Or([
        z.And([P[0] == 2, P[1] == 0]),
        z.And([P[1] == 2, P[2] == 0]),
        z.And([P[2] == 2, P[3] == 0]),
    ])
)

#3. Yvonne was from Stanton.
slv.add(T[3] == 3)

#4. The solver from Fowler finished 2 places before the person from Lytton.
# Thus, either Fowler was 0 and Lytton was 2, or Fowler was 1 and Lytton was 3
slv.add(
    z.Or([
        z.And([P[0] == 1, P[2] == 2]),
        z.And([P[1] == 1, P[3] == 2]),
    ])
)

#5. The contestant from Fowler finished sometime before Kelvin.
# Thus, either Fowler was 0 and Kelvin was 1, or Fowler 1 and Kelvin 2, o
# Fowler 2 and Kelvin 3
# slv.add(C[T[1]] < C[0])
slv.add(
    z.Or(
        z.And([P[0] == 1, C[0] == 1]),
        z.And([P[1] == 1, C[0] == 2]),
        z.And([P[2] == 1, C[0] == 3]),
    )
)

# See if there's a solution or report that it's unsatisfiable.
slv.check()
try:
    model = slv.model()
    solT = [model.eval(T[i]).as_long() for i in xrange(SIZE)]
    solC = [model.eval(C[i]).as_long() for i in xrange(SIZE)]
    solP = [model.eval(P[i]).as_long() for i in xrange(SIZE)]
    print "T = {}, C = {}, P = {}".format(solT, solC, solP)

except Z3Exception:
    print "UNSAT"

