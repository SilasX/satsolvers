import z3.z3 as z
from z3.z3types import Z3Exception


# Initialize the solver
slv = z.Solver()

# Define variables and their types
x = z.Int('x')
y = z.Int('y')

# Solve x * y = 30 and x + y < 12 for positive integers x, y

# Add constraints

slv.add(x > 0)
slv.add(y > 0)
slv.add(x + y < 12)
slv.add(x * y == 30)

# See if there's a solution or report that it's unsatisfiable.
slv.check()
try:
    model = slv.model()
    xout = model.eval(x).as_long()
    yout = model.eval(y).as_long()
    print "x = {}, y = {}".format(xout, yout)

except Z3Exception:
    print "UNSAT"

