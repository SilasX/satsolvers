import z3.z3 as z
from z3 import Distinct
from z3.z3types import Z3Exception

slv = z.Solver()
SIZE = 9

A = [ 
    [z.Int('c{}{}'.format(i, j)) for j in xrange(SIZE)]
    for i in xrange(SIZE)
]

# Ensure vals are in range
for i in xrange(SIZE):
    for j in xrange(SIZE):
        slv.add(A[i][j] > 0)
        slv.add(A[i][j] < 10)

# Ensure distct along row
for i in xrange(9):
    slv.add(Distinct(*[A[i][j] for j in xrange(SIZE)]))

# Ensure distinct along column
for j in xrange(9):
    slv.add(Distinct(*[A[i][j] for i in xrange(SIZE)]))

# Ensure distinct within box
for i in xrange(3):
    for j in xrange(3):
        slv.add(
            Distinct(
                A[3*i][3*j],
                A[3*i+1][3*j],
                A[3*i+2][3*j],
                A[3*i][3*j+1],
                A[3*i+1][3*j+1],
                A[3*i+2][3*j+1],
                A[3*i][3*j+2],
                A[3*i+1][3*j+2],
                A[3*i+2][3*j+2],
            )
        )

#    0 1 2|3 4 5|6 7 8|
# 0  ? ? 9|8 5 6|? ? ?|
# 1  ? 8 ?|? ? 9|? ? ?|
# 2  2 ? ?|? ? 7|? ? ?|
#    -----------------|
# 3  7 ? ?|? ? 1|3 9 6|
# 4  9 ? ?|? 6 ?|? ? 5|
# 5  5 3 6|2 ? ?|? ? 7|
#    -----------------|
# 6  ? ? ?|9 ? ?|? ? 1|
# 7  ? ? ?|3 ? ?|? 6 ?|
# 8  ? ? ?|6 8 2|4 ? ?|
#    -----------------|

slv.add(9 == A[0][2])
slv.add(8 == A[0][3])
slv.add(5 == A[0][4])
slv.add(6 == A[0][5])
slv.add(8 == A[1][1])
slv.add(9 == A[1][5])
slv.add(2 == A[2][0])
slv.add(7 == A[2][5])
slv.add(7 == A[3][0])
slv.add(1 == A[3][5])
slv.add(3 == A[3][6])
slv.add(9 == A[3][7])
slv.add(6 == A[3][8])
slv.add(9 == A[4][0])
slv.add(6 == A[4][4])
slv.add(5 == A[4][8])
slv.add(5 == A[5][0])
slv.add(3 == A[5][1])
slv.add(6 == A[5][2])
slv.add(2 == A[5][3])
slv.add(7 == A[5][8])
slv.add(9 == A[6][3])
slv.add(1 == A[6][8])
slv.add(3 == A[7][3])
slv.add(6 == A[7][7])
slv.add(6 == A[8][3])
slv.add(8 == A[8][4])
slv.add(2 == A[8][5])
slv.add(4 == A[8][6])
# This line makes it fail.
# slv.add(9 == A[8][8])

slv.check()
try:
    model = slv.model()
    solution = [[model.eval(A[i][j]).as_long() for j in xrange(SIZE)] for i in xrange(SIZE)]
    print "Solution :"
    for row in xrange(SIZE):
        print solution[row]

except Z3Exception:
    print "UNSAT"

